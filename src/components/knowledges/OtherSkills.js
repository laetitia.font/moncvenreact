import React from "react";

function OtherSkills() {
  return (
    <div className="otherSkills">
    <h3>Autres compétences</h3>
      <div className="list">
        <ul>
          <li><i className="fas fa-check-square"></i>Power Point</li>
          <li><i className="fas fa-check-square"></i>Traitement de texte, publipostage, Word, works, Excel</li>
          <li><i className="fas fa-check-square"></i>Gestionnaire BDD : Filemaker Pro, Access</li>
          <li><i className="fas fa-check-square"></i>Logiciels comptables : Sage, Ciel Compta, Cogilog, EBP Comptabilité</li>
          <li><i className="fas fa-check-square"></i>Rapidité d'adaptation dans l'apprentissage d'autres technologies, esprit d'équipe</li>
        </ul>
      </div>
    </div>
  );
}

export default OtherSkills;
