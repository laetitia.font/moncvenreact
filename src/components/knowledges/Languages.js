import React, { Component } from 'react';
import ProgressBar from './ProgressBar';


export default class Languages extends Component {
  state = {
    languages: [
      {id: 1, value: "HTML", xp:1.5},
      {id: 2, value: "CSS", xp:1.5},
      {id: 3, value: "Javascript", xp:1.2},
      {id: 4, value: "PHP", xp:1.2},
      {id: 5, value: "MySql", xp:1.7}
    ],
    frameworks: [
      {id: 1, value: "Symfony", xp:1.7},
      {id: 2, value: "React", xp:1.5},
      {id: 3, value: "Angular", xp:1},
      {id: 4, value: "Bootstrap", xp:1.5},
      {id: 5, value: "Wordpress", xp:0.6}
    ],
    concepts: [
      {id: 1, value: "Git, Git Bash, Cmder, Visual Studio, PHP Storm, WebStorm", xp:1.7},
      {id: 2, value: "Webservice REST", xp:1.5},
      {id: 3, value: "Programmation orientée Objet", xp:1},
      {id: 4, value: "Modélisation, UML, établir Cahier des Charges", xp:1.5},
      {id: 5, value: "Notions de SEO", xp:0.6}
    ]
  }

  render() {
    let {languages, frameworks, concepts} = this.state;

    return (
      <div className="languagesFrameworks">
        <ProgressBar
          languages={languages}
          title="languages"
          className="languageDisplay"
        />

        <ProgressBar
          languages={frameworks}
          title="frameworks"
          className="frameworksDisplay"
        />

        <ProgressBar
          languages={concepts}
          title="concepts et outils"
          className="conceptDisplay"
        />
       </div>
     );
   }
 }