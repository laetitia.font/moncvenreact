import React from 'react';

const Career = () => {
    return (
        <div className="career">
            <h3>Parcours</h3>
            <span>
                Au cours de mon expérience au
                poste de secrétaire comptable, j'ai
                appris en autonomie à créer des
                applications de bureau à l'aide d un
                Framework (FILEMAKER PRO), afin
                de répondre aux attentes de
                l'entreprise. Passionnée par
                l'algorithmie et la création
                d'utilitaires de travail, j'ai décidé de
                me réorienter professionnellement,
                afin d'acquérir des compétences
                dans le développement web et
                applications. Consciencieuse et
                enthousiaste, j'ai fait un stage, afin de me professionnaliser BLABLABLABLA.
            </span>
        </div>
    );
};
export default Career;