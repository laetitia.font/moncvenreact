import React from 'react';

const Hobbies = () => {
    return (
        <div className="hobbies">
            <h3>Intérêts</h3>
            <ul>
                <li className="hobby">
                    <i class="fas fa-child"></i>
                    <span>Danses Latines</span>
                </li>
                <li className="hobby">
                    <i class="fas fa-laptop"></i>
                    <span>Création applis de bureau
                    (Filemaker Pro)</span>
                </li>
            </ul>
        </div>
    );
};
export default Hobbies;