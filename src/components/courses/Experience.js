import React from "react";

const Experience = () => {
  return (
    <div className="experience">
      <div className="experienceContent">
        <div className="content">
          <h3>Expérience</h3>
          <div className="exp-1">
            <h4>2021, IDEM</h4>
            <p>Formation développeur Web et mobile Fullstack</p>
          </div>
          <div className="exp-2">
            <h4>2002 à 2021 THERMIDOR</h4>
            <p>Secrétaire comptable, entreprise de dépannage et entretien de chaudières, climatiseurs, pompe à chaleur...</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Experience;
