import React from 'react';

const Qualifications = () => {
    return (
        <div className="qualification">
            <h3>Diplômes</h3>
            <div className="qualificationContent">
                <div className="content">
                    <ul>
                        {/* <li>
                            <h4>Titre Professionnel Développeur Web et mobile Fullstack</h4>
                            <span>L'IDEM, Le Soler</span>
                        </li> */}
                        <li>
                            <h4>BAC PRO COMPTABILITE</h4>
                            <span>Greta Catalogne, Perpignan</span>
                        </li>
                        <li>
                            <h4>BEP METIER DU SECRETARIAT</h4>
                            <span>Lycée Aristide Maillol, Perpignan</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};
export default Qualifications;


// const Qualifications = () => {
//   return (
//     <div className="qualification">
//       <div className="qualificationContent">
//         <div className="content">
//           <h1>Page Diplômes</h1>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default Qualifications;
