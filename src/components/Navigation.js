import React from 'react';
import { NavLink } from 'react-router-dom'

const Navigation = () => {
    return (
        <div className="sidebar">
            <div className="id">
                <div className="idContent">
                    <img src="./media/mini_photo_cv.jpg" alt="profil-pic"/>
                    <h3>Développeur Web Fulstack</h3>
                    <h4>Laëtitia FONT</h4>
                </div>
            </div>

            <div className="navigation">
                <ul>
                    <li>
                        <NavLink exact to="/" activeClassName="navActive">
                            <i className="fas fa-home"></i>
                            <span> Accueil</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/competences" activeClassName="navActive">
                            <i className="fas fa-mountain"></i>
                            <span> Compétences</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/parcours" activeClassName="navActive">
                            <i className="fas fa-prescription-bottle"></i>
                            <span> Mon parcours</span>
                        </NavLink>
                    </li>
                    {/* <li>
                        <NavLink exact to="/diplomes" activeClassName="navActive">
                            <i className="far fa-file"></i>
                            <span> Diplômes</span>
                        </NavLink>
                    </li> */}
                    <li>
                        <NavLink exact to="/contact" activeClassName="navActive">
                            <i className="fas fa-address-book"></i>
                            <span> Contact</span>
                        </NavLink>
                    </li>
                    {/* <li>
                        <NavLink exact to="/portfolio" activeClassName="navActive">
                            <i className="fas fa-images"></i>
                            <span> Portfolio</span>
                        </NavLink>
                    </li> */}
                </ul>
            </div>

            <div className="socialNetwork">
                <ul>
                    <li>
                        <a href="https://www.linkedin.com/in/laetitia-font-7882151b0" target="_blank" rel="noopener noreferrer" ><i className="fab fa-linkedin"></i></a>
                    </li>
                    <li>
                        <a href="https://gitlab.com/laetitia.font" target="_blank" rel="noopener noreferrer" ><i className="fab fa-gitlab"></i></a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/crea.infor.7" target="_blank" rel="noopener noreferrer" ><i className="fab fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="https://twitter.com/CrInformatique1" target="_blank" rel="noopener noreferrer" ><i className="fab fa-twitter"></i></a>
                    </li>
                </ul>
                <div className="signature">
                    <p>Laëtitia FONT - 2021</p>
                </div>
            </div>
        </div>
    );
};

export default Navigation;
