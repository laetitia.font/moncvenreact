import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import Contact from './pages/Contact';
import Courses from './pages/Courses';
// import Qualifications from './pages/Qualifications';
import Knowledges from './pages/Knowledges';
import NotFound from './pages/NotFound';
// import Portfolio from './pages/Portfolio';


// Mise en place Routage des pages
const App = () => {
    return (
        <>
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/contact" component={Contact} />
                    <Route path="/competences" component={Knowledges} />
                    {/* <Route path="/diplomes" component={Qualifications} /> */}
                    <Route path="/parcours" component={Courses} />
                    {/* <Route path="/portfolio" component={Portfolio} /> */}
                    <Route component={NotFound} />
                </Switch>
            </BrowserRouter>
        </>
    );
};

export default App;
