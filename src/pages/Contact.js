import React from "react";
import Navigation from "../components/Navigation";
import { CopyToClipboard } from 'react-copy-to-clipboard';

const Contact = () => {
  return (
    <div className="contact">
      <Navigation />
      <div className="contactContent">
      <div className="header"></div>
        <div className="contactBox">
          <h1>Contactez-moi</h1>
          <ul>
            <li>
              <i className="fas fa-map-marker-alt"></i>
              <span>Toulouges</span>
            </li>
            <li>
              <i className="fas fa-mobile-alt"></i>
              <CopyToClipboard text="0695099876">
                <span 
                  className="clickInput" 
                  onClick={() => {alert('Téléphone copié !');}}>06 95 09 98 76
                </span>
                </CopyToClipboard>
            </li>
            <li>
              <i className="fas fa-envelope"></i>
              <CopyToClipboard text="fontlaetitia66@gmail.com">
              <span 
                className="clickInput" 
                onClick={() => {alert('Email copié !');}}>fontlaetitia66@gmail.com
              </span>
              </CopyToClipboard>
            </li>
            <li>
              <i class="fas fa-car"></i>
                <span>Permis B + Véhicule</span>
            </li>
          </ul>
        </div>
        <div className="socialNetwork">
          <ul>
              <li>
                  <a href="https://www.linkedin.com/in/laetitia-font-7882151b0" target="_blank" rel="noopener noreferrer" ><i className="fab fa-linkedin"></i>
                  </a>
              </li>
              <li>
                  <a href="https://gitlab.com/laetitia.font" target="_blank" rel="noopener noreferrer" ><i className="fab fa-gitlab"></i>
                  </a>
              </li>
              <li>
                  <a href="https://www.facebook.com/crea.infor.7" target="_blank" rel="noopener noreferrer" ><i className="fab fa-facebook"></i>
                  </a>
              </li>
              <li>
                  <a href="https://twitter.com/CrInformatique1" target="_blank" rel="noopener noreferrer" ><i className="fab fa-twitter"></i>
                  </a>
              </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Contact;
