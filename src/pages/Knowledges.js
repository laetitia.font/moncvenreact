import React from "react";
import Navigation from "../components/Navigation";
import Languages from '../components/knowledges/Languages';
import OtherSkills from '../components/knowledges/OtherSkills';

const Knowledges = () => {
  return (
    <div className="knowledges">
      <Navigation />
      <div className="knowledgesContent">
          <h1>Compétences</h1>
        <div className="content">
              <Languages />
              <OtherSkills />
        </div>
       </div>
    </div>
  );
}

export default Knowledges;
