import React from 'react';
import Navigation from '../components/Navigation';


const Portfolio = () => {
    return (
        <div className="portfolio">
            <Navigation />
            <div className="portfolioContent">
                <div className="content">
                    <h1>Portfolio</h1>
                </div>
            </div>
        </div>
    );
};

export default Portfolio;
