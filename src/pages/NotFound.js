import React from 'react';
import Navigation from '../components/Navigation';


const NotFound = () => {
    return (
        <div className="notFound">
            <Navigation />
            <div className="notFoundContent">
                <div className="content">
                    <h1>Oupssss.... la page que vous demandez n'existe pas</h1>
                </div>
            </div>
        </div>
    );
};

export default NotFound;
