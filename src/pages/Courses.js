import React from "react";
import Navigation from "../components/Navigation";
import Experience from '../components/courses/Experience';
import Hobbies from '../components/courses/Hobbies';
import Qualifications from '../components/courses/Qualifications';
import Career from '../components/courses/Career';


const Courses = () => {
  return (
    <div className="courses">
      <Navigation />
      <div className="coursesContent">
        <div className="content">
          <h1>Mon parcours</h1>
          <Experience />
          <Qualifications />
          <Career />
          <Hobbies />
        </div>
      </div>
    </div>
  );
};

export default Courses;
