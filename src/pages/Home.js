import React from 'react';
import Navigation from '../components/Navigation';

const Home = () => {
    return (
        <div className="home">
            <Navigation />
            <div className="homeContent">
                <div className="content">
                    <h1>Développeur Web Fullstack</h1>
                    <h2>Laëtitia FONT</h2>
                    <div className="pdf">
                        <a href="./media/CvPdf.pdf" target="_blank">Télécharger CV pdf</a>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;
